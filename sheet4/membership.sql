CREATE DATABASE IF NOT EXISTS member;

CREATE TABLE IF NOT EXISTS UserDetail (
    User_ID int(11) NOT NULL AUTO_INCREMENT,
    User_name varchar(50) NOT NULL,
    User_lname varchar(50) NOT NULL,
    User_addr varchar(100) NOT NULL,
    User_email varchar(50) NOT NULL,
    User_tel int(11) NOT NULL,
    Username varchar(50) NOT NULL,
    password varchar(50) NOT NULL,
    PRIMARY KEY(id)
    );